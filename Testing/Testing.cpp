// Testing.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "gtest\gtest.h"
#include "..\ConsoleApplication1\List.h"

TEST(ListTesting, PushBack)
{
	const int SIZE = 30;
	int* m = new int[SIZE];
	for (int i = 0; i < SIZE;++i)
		m[i]= (rand());
	List<int> numbers;
	for (int i = 0; i < SIZE; i++)
		numbers.push_back(m[i]);
	auto it = numbers.begin();
	for (int i = 0; i<SIZE; ++i,++it)
		EXPECT_DOUBLE_EQ(m[i], *it);
}

TEST(ListTesting, Clear_Empty)
{
	const int SIZE = 10;
	int* m = new int[SIZE];
	for (int i = 0; i < SIZE; ++i)
		m[i] = (rand());
	List<int> numbers;
	for (int i = 0; i < SIZE; i++)
		numbers.push_back(m[i]);
	numbers.clear();
	ASSERT_EQ((numbers.begin())== 0 , true);
	numbers.push_back(5);
	ASSERT_EQ((numbers.begin()) == 0, false);
}

TEST(ListTesting, Remove)
{
	int a[]{9, 5, 6, 1};
	List<int> numbers;
	for (int i = 0; i < (sizeof(a) / sizeof(a[0])); ++i)
		numbers.push_back(a[i]);
	List<int>::ListIt it = numbers.begin();
	numbers.remove(20);
	for (int i = 0; i < (sizeof(a) / sizeof(a[0])); ++i, ++it)
		EXPECT_DOUBLE_EQ(a[i], *it);
	numbers.remove(5);
	it = numbers.begin();
	EXPECT_DOUBLE_EQ(a[0], *it);
	it++;
	EXPECT_DOUBLE_EQ(a[2], *it);
	it++;
	EXPECT_DOUBLE_EQ(a[3], *it);
	numbers.push_back(10);
	numbers.remove(10);
	it = numbers.begin();
	EXPECT_DOUBLE_EQ(a[0], *it);
	it++;
	EXPECT_DOUBLE_EQ(a[2], *it);
	it++;
	EXPECT_DOUBLE_EQ(a[3], *it);
}

TEST(ListTesting, Front)
{
	int a[]{9, 5, 6, 1, 7, 7, 12, 1, 98, 8,12};
	List<int> numbers;
	for (int i = 0; i < (sizeof(a) / sizeof(a[0])); ++i)
		numbers.push_back(a[i]);
		ASSERT_EQ(numbers.front(), 9);
		numbers.remove(9);
		ASSERT_EQ(numbers.front(), 5);
}

TEST(ListTesting, PopFront)
{
	int a[]{9, 5, 6, 1, 7, 7, 12, 1, 98, 8, 12};
	List<int> numbers;
	for (int i = 0; i < (sizeof(a) / sizeof(a[0])); ++i)
		numbers.push_back(a[i]);
	numbers.pop_front();
	ASSERT_EQ(numbers.front(), 5);
}

TEST(ConstListTesting, out)
{
	int a[]{9, 5, 6, 1, 7, 7, 12, 1, 98, 8, 12};
	List<int> numbers;
	for (int i = 0; i < (sizeof(a) / sizeof(a[0])); ++i)
		numbers.push_back(a[i]);
	for (auto it = numbers.Cbegin(); it != numbers.Cend(); ++it)
		std::cout << *it << std::endl;
}


int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0;
}