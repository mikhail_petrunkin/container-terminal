#pragma once

#include "Box.h"

/**����������� �����, ����������� ����������� ���������*/
class CooledBox : virtual public Box
{
public:
	CooledBox(Point size, float m, const std::string& firm, int price, int t);
	Box* Clone() const;
	bool CheckT(int t) const;
	int Type() const;
	std::ostream& PrintAdd(std::ostream& os) const;
protected:
	int temperature; ///>����������� ���������� �����������
};

