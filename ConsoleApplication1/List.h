#pragma once

/**������ ������*/
template <class T>
class List
{
private:
	struct ListElem;
	ListElem* head;
	ListElem* back;
public:
	List();
	~List();
	T front() const;
	void push_back(const T data);
	void clear();
	void remove(T data);
	void pop_front();
	bool empty() const;
	void print();
	class ListIt;
	class constListIt;
	ListIt begin();
	ListIt end();
	constListIt Cbegin();
	constListIt Cend();
};

template<class T>
struct List<T>::ListElem
{
	T data;
	ListElem* next;
};

template<class T>
List<T>::List()
{
	head = nullptr;
}

template<class T>
List<T>::~List()
{
	clear();
}

template<class T>
T List<T>::front() const
{
	return head->data;
}

template<class T>
void List<T>::push_back(const T data)
{
	ListElem* temp = new ListElem;
	if (head == nullptr)
		head = temp;
	else
		back->next = temp;
	temp->data = data;
	temp->next = nullptr;
	back = temp;
}

template<class T>
void List<T>::clear()
{
	while (head != nullptr)
	{
		ListElem *temp = head;
		head = head->next;
		delete temp;
	}
}

template<class T>
void List<T>::remove(T data)
{
	ListElem *iter = head;
	ListElem *prev = 0;
	while (iter != nullptr)
	{
		ListElem *temp = iter;
		if (temp->data == data)
		{
			if (temp == head) //��������� ������� - ������
			{
				head = temp->next;
				delete temp;
				return;
			}
			prev->next = iter->next;
			if (iter->next == nullptr)
				back = prev;
			delete temp;
			temp = 0;
			return;
		}
		prev = iter;
		iter = iter->next;
	}
}

template<class T>
void List<T>::pop_front()
{
	if (head)
	{
		ListElem* temp = head;
		head = head->next;
		delete temp;
	}
}

template<class T>
bool List<T>::empty() const
{
	if (head == nullptr) return true;
	else return false;
}

template <class T>
void List<T>::print()
{
	ListElem*temp = head;
	while (temp != nullptr)
	{
		std::cout << temp->data << std::endl;;
		temp = temp->next;
	}
}

template <class T>
class List<T>::ListIt
{
private:
	ListElem* cur;
public:
	ListIt(ListElem *ob) : cur(ob){ };
	ListIt() : cur(NULL) { };
	ListIt operator++() ;
	const ListIt operator++(int);
	const bool operator==(const ListIt&) const;
	const bool operator!=(const ListIt&)const;
	T operator*();
};

template<class T>
typename List<T>::ListIt List<T>::begin()
{
	return ListIt(this->head);
}

template<class T>
typename List<T>::ListIt List<T>::end()
{
	return ListIt(nullptr);
}

template<class T>
typename List<T>::ListIt List<T>::ListIt::operator++() //prefix
{
	cur = cur->next;
	return *this;
}

template<class T>
typename const List<T>::ListIt List<T>::ListIt::operator++(int) //postfix
{
	List<T>::ListIt r(*this);
	cur = cur->next;
	return r;
}

template<class T>
T List<T>::ListIt::operator*()
{
	return cur->data;
}

template<class T>
const bool List<T>::ListIt::operator==(const typename List<T>::ListIt& c) const
{
	return cur==c.cur;
}

template<class T>
const bool List<T>::ListIt::operator!=(const typename List<T>::ListIt& c) const
{
	return cur != c.cur;
}

///CONST

template <class T>
class List<T>::constListIt
{
private:
	ListElem* cur;
public:
	constListIt(ListElem *ob) : cur(ob){ };
	constListIt() : cur(NULL) { };
	constListIt operator++();
	const constListIt operator++(int);
	const bool operator==(const constListIt&) const;
	const bool operator!=(const constListIt&) const;
	const T operator*();
};

template<class T>
typename List<T>::constListIt List<T>::Cbegin()
{
	return constListIt(this->head);
}

template<class T>
typename List<T>::constListIt List<T>::Cend()
{
	return constListIt(nullptr);
}

template<class T>
typename List<T>::constListIt List<T>::constListIt::operator++() //prefix
{
	cur = cur->next;
	return *this;
}

template<class T>
typename const List<T>::constListIt List<T>::constListIt::operator++(int) //postfix
{
	List<T>::constListIt r(*this);
	cur = cur->next;
	return r;
}

template<class T>
const T List<T>::constListIt::operator*()
{
	return cur->data;
}

template<class T>
const bool List<T>::constListIt::operator==(const typename List<T>::constListIt& c) const
{
	return cur == c.cur;
}

template<class T>
const bool List<T>::constListIt::operator!=(const typename List<T>::constListIt& c) const
{
	return cur != c.cur;
}