#include "Point.h"
#include "Terminal.h"
#include "Storage.h"

#include <cstring>

using namespace std;

Terminal::Terminal()
{
}

Terminal::~Terminal()
{
	for (auto& storage : storages)
		delete storage;
}

int Terminal::AddStorage(Storage* storage)
{
	auto empty = find(storages.begin(), storages.end(), nullptr);
	if (empty == storages.end())
	{
		storages.push_back(storage);
		return storages.size() - 1;
	}

	*empty = storage;
	return empty - storages.begin();
}

bool Terminal::RemoveStorage(int id)
{
	if (id < 0 || id >= storages.size())
		return false;

	if (storages[id])
	{
		delete storages[id];
		storages[id] = 0;
	}
	return true;
}

Storage* Terminal::GetStorage(int id) const
{
	if (id < 0 || id >= storages.size())
		return 0;

	return storages[id];
}

//bool Terminal::SetStorageSize(int id, Point size)
//{
//	if (!GetStorage(id))
//		return false;
//	return GetStorage(id)->Resize(size);
//}

ostream& operator << (ostream& os, const Terminal& term)
{
	os << "Storages in Terminal:" << endl;
	for (int i = 0; i < term.storages.size(); ++i)
		if (term.storages[i])
			cout << "[" << i << "] " << term.storages[i]->GetSize() << 
			" " << term.storages[i]->GetTemperature() << endl;

	return os;
}