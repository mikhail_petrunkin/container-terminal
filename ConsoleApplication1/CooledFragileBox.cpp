#include "CooledFragileBox.h"

CooledFragileBox::CooledFragileBox(Point size, float m, const std::string& firm, int price, float maxUpperMass, int t)
	: CooledBox(size, m,firm,price, t)
	, FragileBox(size, m, firm,price, maxUpperMass)
	,Box(size, m, firm, price)
{

}

Box* CooledFragileBox::Clone() const
{
	return new CooledFragileBox(*this);
}

int CooledFragileBox::Type() const
{
	return 3;
}

std::ostream& CooledFragileBox::PrintAdd(std::ostream& os) const
{
	os  << temperature << " " << maxUpperMass;
	return os;
}


