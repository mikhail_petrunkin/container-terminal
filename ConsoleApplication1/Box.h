#pragma once

#include <string>
#include "Storage.h"

/**�����, ����������� ������� ���������*/
class Box
{
public:
	/*!
	���������� ���������� ������
	\param size ������
	\param m �����
	\param firm �������� �����
	\param price ���������
	*/
	Box(Point size, float m, const std::string& firm, int price);
	virtual ~Box(){}; ///<���������� ���������� ������
	Point GetRotatedWHD() const; ///<��������� ��������� ���������� � ������ ��� �������� \return ���������� ����������
	virtual Box* Clone() const; ///<�������� ����� \return ������ �� ������������� ��������� ������
	int GetID() const; ///<����������� id ���������� \return id ����������
	virtual bool CheckT(int t) const; ///<�������� ���������� ����������� \return true, ���� ����������� �� ��������� ����������
	virtual bool CheckUpperMass(float m) const; ///<�������� ���������� ����� \return true, ���� ����� �� ��������� ����������
	float GetDensity() const;  ///<��������� ��������� ���������� \return �������� ���������
	void GetAABB(Point& a, Point& b) const; ///<��������� ������� ����� ���������� \param a,b ������� �����
	Point GetPosition() const; ///<��������� ��������� ���������� \return ��������� ����������
	Point GetRotation() const; ///<��������� �������� ���������� \return �������� ����������
	virtual void SetRotation(Point angle); ///<������� �������� ���������� \param angle ��������
	void SetPosition(Point p); ///<������� ��������� ���������� \param p ���������
	void SetID(int id); ///<������� id ���������� \param id id
	/*!
	\brief ����������� ���� ����������
	0-�������
	1-�����������
	2-�������
	3-������� �����������
	*/
	virtual int Type() const;
	std::ostream& Print(std::ostream& os) const; ///< �������� ������ ���������� � ���������� (� ����������� �� ��� ����) � ����� \param os ������ �� ����� \return ������ �� �������� �����
protected:
	int id =0 ; //id=-1 ///<id
	Point pos;///<���������
	Point size;///<������
	Point rotation;///<��������
	float m; ///<�����
	std::string firm;///<�������� �����
	int price;///<���������
	virtual std::ostream& PrintAdd(std::ostream& os) const; ///<������� ������ ���������� � ���������� � ����� \param os ������ �� ����� \return ������ �� �������� �����
};

/**����������� �����, ����������� ����������� ���������*/
class NoRotBox : public Box
{
public:
	NoRotBox(Point size, float m, const std::string& firm, int price)
		: Box(size, m,firm,price)
	{}
	virtual void SetRotation(Point angle) override {};
};
