#include "FragileBox.h"

FragileBox::FragileBox(Point size, float m, const std::string& firm, int price, float maxUpperMass)
	: Box(size, m,firm,price)
	, maxUpperMass(maxUpperMass)
{

}

Box* FragileBox::Clone() const
{
	return new FragileBox(*this);
}

bool FragileBox::CheckUpperMass(float m) const
{
	if (maxUpperMass <= m)
		return true;
	else

		return false;
}

void FragileBox::SetRotation(Point angle)
{
	rotation.z = angle.z;
}

int FragileBox::Type() const
{
	return 2;
}

std::ostream& FragileBox::PrintAdd(std::ostream& os) const
{
	os << "No " << maxUpperMass;
	return os;
}

