#pragma once

#include <iostream>

struct Point
{
	int x, y, z;

	Point() ///<����������� �� ��������� 
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Point(int a, int b, int c)  ///<����������� � ����������� \param a,b,c ����������
	{
		x = a;
		y = b;
		z = c;
	}

	Point operator +(const Point& rhs) const ///<�������� �������� ����� \param rhs ������ �� ������ ����� \return ��������� ��������
	{
		Point p = *this;
		p.x += rhs.x;
		p.y += rhs.y;
		p.z += rhs.z;
		return p;
	}

	Point operator *(int f) const ///<�������� ��������� ����� �� ����� \param f ��������� \return ��������� ��������
	{
		Point p = *this;
		p.x *= f;
		p.y *= f;
		p.z *= f;
		return p;
	}

	Point operator -(const Point& rhs) const ///<�������� ��������� ����� \param rhs ������ �� ������ ����� \return ��������� ��������
	{
		return *this + rhs * (-1);
	}

	/*!
	�������� ����� ����� �� ������
	\param os ������ �� �����
	\param p ������ �� ���������� �����
	\return ������ �� �������� �����
	*/
	friend std::ostream& operator << (std::ostream& os, const Point& p)
	{
		os << "(" << p.x << ", " << p.y << ", " << p.z << ")";
		return os;
	}

	/*!
	�������� ������ ����� � �����
	\param os ������ �� �����
	\param p ������ �� ���������� �����
	\return ������ �� �������� �����
	*/
	friend std::istream& operator >> (std::istream& is, Point& p)
	{
		std::cout << "x: ";
		is >> p.x;
		std::cout << "y: ";
		is >> p.y;
		std::cout << "z: ";
		is >> p.z;
		return is;
	}
};

