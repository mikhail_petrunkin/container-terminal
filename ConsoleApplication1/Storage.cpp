#include "Storage.h"
#include "Box.h"

#include <cassert>
#include <algorithm>

using namespace std;

Storage::Storage(Point sz, int temp)
	: t(temp)
	, size(0, 0, 0)
{
	Resize(sz);
	CalculateSpace();
};

Storage::~Storage()
{
	for (auto it = boxes.begin(); it != boxes.end(); ++it)
		delete *it;

	if (space)
	{
		for (int i = 0; i < size.x; ++i)
		{
			for (int j = 0; j < size.y; ++j)
			{
				delete[] space[i][j];
			}
			delete[] space[i];
		}
		delete[] space;
	}
}

bool Storage::MoveBox(int id, Point pos)
{
	Box* box = GetBox(id);
	if (box == 0)
		return false;
	return MoveAndRotateBox(id, pos, (box)->GetRotation());
}

bool Storage::RotateBox(int id, Point rot)
{
	Box* box = GetBox(id);
	if (box == 0)
		return false;
	return MoveAndRotateBox(id, box->GetPosition(), rot);
}

bool Storage::MoveAndRotateBox(int id, Point pos, Point rot)
{
	CalculateSpace();

	Box* box = GetBox(id);
	if (box == 0)
		return false;

	List<Box*> cloneBoxes;
	for (auto it = cloneBoxes.begin(); it != cloneBoxes.end(); ++it)
		cloneBoxes.push_back((*it)->Clone());
	auto cloneBox = box->Clone();
	RemoveBox(id);

	CalculateSpace();
	if (AddBox(cloneBox, pos, rot) != -1) //���� ���������� ���������
	{
		for (auto it = cloneBoxes.begin(); it != cloneBoxes.end(); ++it)
			delete *it;

		return true;
	}

	for (auto it = boxes.begin(); it != boxes.end(); ++it)
		delete *it;

	boxes.clear();

	for (auto it = cloneBoxes.begin(); it != cloneBoxes.end(); ++it)
		boxes.push_back(*it);

	return false;
}

bool Storage::Resize(Point sz)
{
	Point szdiff = sz - size;
	if (szdiff.x < 0 ||
		szdiff.y < 0 ||
		szdiff.z < 0)
	{
		int x1 = 0;
		int y1 = 0;
		int z1 = 0;

		int x2 = size.x;
		int y2 = size.y;
		int z2 = size.z;

		if (szdiff.x < 0)
			x1 = x2 + szdiff.x;

		if (szdiff.y < 0)
			y1 = y2 + szdiff.y;

		if (szdiff.z < 0)
			z1 = z2 + szdiff.z;

		for (int i = 0; i < x2; i++)
			for (int j = 0; j < y2; j++)
				for (int k = 0; k < z2; k++)
				{
					if (i >= x1 ||
						j >= y1 ||
						k >= z1)
						if (space[i][j][k])
							return false;
				}
	}


	int *** newspace = new int**[sz.x];
	for (int i = 0; i < sz.x; ++i)
	{
		newspace[i] = new int*[sz.y];
		for (int j = 0; j < sz.y; ++j)
			newspace[i][j] = new int[sz.z];

	}

	if (space)
	{
		for (int i = 0; i < size.x; ++i)
		{
			for (int j = 0; j < size.y; ++j)
			{
				delete[] space[i][j];
			}
			delete[] space[i];
		}
		delete[] space;
	}

	space = newspace;

	size = sz;
	CalculateSpace();

	return true;

}

void Storage::CalculateSpace()
{
	for (int i = 0; i < size.x; ++i)
		for (int j = 0; j < size.y; ++j)
			for (int k = 0; k < size.z; ++k)
				space[i][j][k] = 0;

	for (auto box = boxes.begin(); box != boxes.end(); ++box)
	{
		bool testRes = TestBoxIntoSpace(*box);
		assert(testRes);
		PutBoxIntoSpace(*box);
	}

}

bool Storage::TestBoxIntoSpace(const Box* box) const
{
	Point a, b;
	box->GetAABB(a, b);
	//std::cout << "GetAABB(TestBoxIntoSpace) " << a << "  " << b;
	//system("pause");
	if (a.x < 0 || a.y < 0 || a.z < 0 || a.x >= size.x || a.y >= size.y || a.z >= size.z)
		return false;
	if (b.x < 0 || b.y < 0 || b.z < 0 || b.x >= size.x || b.y >= size.y || b.z >= size.z)
		return false;
	for (int i = a.x; i < b.x; ++i)
	{
		if (i < 0 || i >= size.x)
		{
			//	std::cout << "1";
			return false;
		}

		for (int j = a.y; j < b.y; ++j)
		{
			if (j < 0 || j >= size.y)
			{
				//	std::cout << "2";
				return false;
			}
			for (int k = a.z; k < b.z; ++k) //��� ����������� ������ ���� �� �����
			{
				if (k < 0 || k >= size.z)
				{
					//	std::cout << "3";
					return false;
				}
				/*	std::cout << i << " " << j << " " << k << endl;
					system("pause");*/
				if (space[i][j][k])
				{
					//	std::cout << "4";
					return false;
				}
			}
		}
	}
	return true;
}

void Storage::PutBoxIntoSpace(const Box* box)
{
	Point a, b;
	box->GetAABB(a, b);
	int id = box->GetID();

	for (int i = a.x; i < b.x; ++i)
	{
		if (i < 0 || i >= size.x)
		{
			assert(false);
		}


		for (int j = a.y; j < b.y; ++j)
		{
			if (j < 0 || j >= size.y)
			{
				assert(false);
			}


			for (int k = a.z; k < b.z; ++k)
			{
				if (k < 0 || k >= size.z)
				{
					assert(false);
				}
				/*if (space[i][j][k])
					assert(false);*/

				space[i][j][k] = id;
				//	std::cout << i << " " << j << " " << k << " " << " " << box->GetID() << std::endl;
			}
		}
	}
}


void Storage::RemoveBox(int id)
{
	List<Box*> upperBoxes;
	TemporaryRemoveUpperBox(id, upperBoxes);
	delete upperBoxes.front();
	upperBoxes.pop_front();

	CalculateSpace();

	for (auto it = upperBoxes.begin(); it != upperBoxes.end(); ++it)
		AddBox(*it);
	CalculateSpace();
}

void Storage::TemporaryRemoveUpperBox(int id, List<Box*>& toAdd)
{
	Box* box = GetBox(id);
	if (box == 0)
		return;
	toAdd.push_back(box);
	boxes.remove(box);
	CalculateSpace();

	Point a, b;
	GetVolumeAboveAABB(box, a, b);
	//std::cout << a << " above AB " << b << endl;
	for (int i = a.x; i < b.x; ++i)
		for (int j = a.y; j < b.y; ++j)
			for (int k = a.z; k < b.z; ++k)
				if (space[i][j][k])
					TemporaryRemoveUpperBox(space[i][j][k], toAdd);
}

int Storage::DoAddBox(Box* box, Point pos, Point eulerRot)
{
	box->SetPosition(pos);
	box->SetRotation(eulerRot);
	int type = box->Type();
		//std::cout<<box->Type();
	//	std::cout << "AddBox WORK";
	if (!TestBoxIntoSpace(box))
		return -1;
	//	std::cout << "TestBoxIntoSpace OK!";
	if (!box->CheckT(t))
		return -1;
	//	std::cout << "CheckT OK!";
		for (auto box = boxes.begin(); box !=boxes.end();++box)
		{
			//(*box)->Print(std::cout);
			if (!CheckUpperMass(*box))
				return -1;
		}
	//	std::cout << "CheckUpperMass OK!";
	if (!CheckPyramid(box))
		return -1;
	//	std::cout << "CheckPyramid OK!";
	if (!box->GetID())
		box->SetID(boxesIDsCounter++);

	boxes.push_back(box);

	CalculateSpace();

	return box->GetID();
}

int Storage::AddBox(Box* box, Point pos, Point eulerRot)
{
	CalculateSpace();
	return DoAddBox(box, pos, eulerRot);
}

int Storage::AddBox(Box* box)
{
	Point p;
	Point rot;
	CalculateSpace();
	for (p.z = 0; p.z < size.z; ++p.z)
		for (rot.x = 0; rot.x < 2; ++rot.x)
			for (rot.y = 0; rot.y < 2; ++rot.y)
				for (rot.z = 0; rot.z < 2; ++rot.z)
					for (p.x = 0; p.x < size.x; ++p.x)
						for (p.y = 0; p.y < size.y; ++p.y)

						{
							int id = DoAddBox(box, p, rot);
							if (id != -1)
							{
								cout << "Success!" << box->GetPosition() << box->GetRotation() << endl;
								return id;
							}
						}
	return -1;
}

void Storage::GetVolumeAboveAABB(const Box* box, Point& a, Point& b) const
{
	Point boxA;
	Point boxB;
	box->GetAABB(boxA, boxB);

	a = boxA;
	a.z = boxB.z;

	b = boxB;
	b.z = size.z;
}

void Storage::GetVolumeBelowAABB(const Box* box, Point& a, Point& b) const
{
	Point boxA;
	Point boxB;
	box->GetAABB(boxA, boxB);

	a = boxA;
	a.z = 0;

	b = boxB;
	b.z = boxA.z - 1; //������ ����� ����� ����� 1
}

bool Storage::CheckUpperMass(const Box* box)
{
	Point a, b;
	GetVolumeAboveAABB(box, a, b);
	float m = 0.0f;
	for (int i = a.x; i < b.x; ++i)
		for (int j = a.y; j < b.y; ++j)
			for (int k = a.z; k < b.z; ++k)
			{
				if (space[i][j][k])
				{
					Box* b = GetBox(space[i][j][k]);
					assert(b);
					m += b->GetDensity();
				}
			}
	//std::cout << "upper mass " << m;
	//system("pause");
	return box->CheckUpperMass(m);
}

Box* Storage::GetBox(int id)
{
	for (auto box = boxes.begin(); box != boxes.end(); ++box)
		if ((*box)->GetID()==id) return *box;
	return 0;
}

bool Storage::CheckPyramid(const Box* box) const
{
	Point a1, b1;
	box->GetAABB(a1, b1);
	//	std::cout <<"GetAABB(CheckPyramid) "<< a1 << "  " << b1;
	if (a1.z == 0) return true;
	Point a, b;
	GetVolumeBelowAABB(box, a, b);
	//	std::cout <<"GetVolumeBelowAABB(CheckPyramid)) "<< a << "  " << b;
	for (int i = a.x; i < b.x; ++i)
	{
		if (i < 0 || i >= size.x)
		{
			assert(false);
		}
		for (int j = a.y; j < b.y; ++j)
		{
			if (j < 0 || j >= size.y)
			{
				assert(false);
			}

			for (int k = a.z; k < b.z; ++k)
			{
				if (k < 0 || k >= size.z)
				{
					assert(false);
				}

				if (!space[i][j][k])
					return false;
			}
		}
	}

	return true;
}

int Storage::GetType(int id)
{
	Box *b = GetBox(id);
	if (b)
		return b->Type();
	return -1;
}

int Storage::GetMaxPossibleCountOf(Point size, bool allowRotation)
{
	if (!boxes.empty())
		return 0;

	Box* newBox = 0;
	int count = 0;
	do
	{
		if (!allowRotation)
		{
			newBox = new NoRotBox(size, 1.0f, "firm", 0);
			std::cout << "��� ��������";
		}
		else
		{
			newBox = new Box(size, 1.0f, "firm", 0);
			std::cout << "� ���������";
		}
		count++;
		std::cout << count << " count " << endl;
	} while (AddBox(newBox) != -1);
	count--;

	delete newBox;
	//for (auto& box : boxes)
	for (auto box = boxes.begin(); box != boxes.end();++box)
		delete *box;

	boxes.clear();

	return count;
}

Point Storage::GetSize() const
{
	return size;
}

int Storage::GetTemperature() const
{
	return t;
}

ostream& operator << (ostream& os, Storage& storage)
{
	os << "Boxes in Storage:" << endl;
	os << "id" << " " << "Position" << " " << "Size" << " " << "Rotation" << " " << "Mass" << " " << "Firm" << " " << "Price" << " " << "MaxT" << " " << "MaxUM" << std::endl;
	for (auto box = storage.boxes.begin(); box != storage.boxes.end(); ++box)
		(*box)->Print(os);
	return os;
}