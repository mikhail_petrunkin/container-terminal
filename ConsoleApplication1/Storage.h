#pragma once

#include "Point.h"
#include "List.h"
class Box;

/**�����, ����������� ����� ���������*/
class Storage
{
public:
	/*!
	���������� ���������� ������
	\param size ������ ������
	\param t ����������� �� ������
	*/
	Storage(Point size, int t);
	~Storage(); ///<���������� ���������� ������
	Box* GetBox(int id); ///<��������� ��������� �� ��������� �� ��� id \param id id ���������� \return ��������� �� ���������
	/*!
	���������� ���������� � ������ ��������� ����������
	\param box ��������� �� ���������
	\param pos ������� ���������� �� ������
	\param eulerRot ��������
	\return id ����������, ���� ���������� ������ �������
	*/
	int AddBox(Box* box, Point pos, Point eulerRot);
	/*!
	���������� ���������� � �������������� ������� ����������� �����
	\param box ��������� �� ���������
	\return id ����������, ���� ���������� ������ �������
	*/
	int AddBox(Box*); 
	void RemoveBox(int id); ///<�������� ���������� �� ������  \param id id ����������
	bool Resize(Point size); ///<��������� ������� ������  \param size ����� ������ \return true, ���� ������ �������
	/*!
	����������� ����������
	\param id id ����������
	\param pos ����� ���������
	\return true, ���� ����������� ���������
	*/
	bool MoveBox(int id, Point pos);
	/*!
	��������������� ������� ����������� � �������� ����������
	\param id id ����������
	\param pos ����� ���������
	\param rot ��������
	\return true, ���� �������� ���������
	*/
	bool MoveAndRotateBox(int id, Point pos, Point rot);
	/*!
	�������� ����������
	\param id id ����������
	\param angle ��������
	\return true, ���� �������� ���������
	*/
	bool RotateBox(int id, Point angle);
	int GetType(int id); ///<����������� ���� ���������� \param id id ���������� \return ����� ���� ����������
	Point GetSize() const; ///<����������� ������� ������ \return ������ ������
	int GetTemperature() const; ///<����������� ����������� �� ������ \return ����������� �� ������
	/*!
	����������� ����������� ���������� ���������� �����������, ������������ �� ������
	\param size ������ ����������
	\param allowRotation ��������� ��� ��������� ��������
	\return ���������� ����������
	*/
	int GetMaxPossibleCountOf(Point size, bool allowRotation);
	/*!
	�������� ������ ���������� � ������ � �����
	\param os ������ �� �����
	\param storage ������ �� �����
	\return ������ �� �������� �����
	*/
	friend std::ostream& operator << (std::ostream& os, Storage& storage);

private:
	/*!
	��������������� �������, ����������� ���������� ����������
	\param box ��������� �� ���������
	\param pos ������� ���������� �� ������
	\param eulerRot ��������
	\return id ����������, ���� ���������� ������ �������
	*/
	int DoAddBox(Box* box, Point pos, Point eulerRot);
	void CalculateSpace(); ///<�������, ����������� ����������� ����� ������� ����������� � id �����������
	bool TestBoxIntoSpace(const Box* box) const; ///<�������� �� ����������� � ��� ���������� ������������ \param box ������ �� ��������� \return true, ���� ����������� �����������
	void PutBoxIntoSpace(const Box* box); ///<���������� � ������ ����������� ���������� ���������� \param box ������ �� ���������
	/*!
	����������� ������������ ��� �����������
	\param box ��������� �� ���������
	\param a,b ������� �����
	*/
	void GetVolumeAboveAABB(const Box* box, Point& a, Point& b) const;
	/*!
	����������� ������������ ��� �����������
	\param box ��������� �� ���������
	\param a,b ������� �����
	*/
	void GetVolumeBelowAABB(const Box* box, Point& a, Point& b) const;
	bool CheckUpperMass(const Box* box); ///<�������� ����� ������� ������ ����������� /param box ������ �� ��������� \return true, ���� ����� �� ��������� ����������
	bool CheckPyramid(const Box* box) const; ///<�������� ���������� "�������" /param box ������ �� ��������� \return true, ���� "��������" �����������
	void TemporaryRemoveUpperBox(int id, List<Box*>& toAdd); ///< ��������� �������� ����������� ��� �������� ����������� \param id ��������� �� ��������� \param toAdd ������ �� ������ �������� �������� �����������
	
	Point size; ///<������ ������
	int t = 0; ///<����������� �� ������
	List<Box*> boxes;///<������ �����������, ����������� �� ������
	int*** space = 0;///<������ ����������� ����� ������� ������������ � id �����������
	int boxesIDsCounter = 1; ///<������� ���������� ����������� �� ������
};