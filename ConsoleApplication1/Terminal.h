#pragma once
#include "Point.h"
#include "Storage.h"
#include <iostream>
#include <vector>


/**�����, ����������� ������������ ��������*/
class Terminal
{
public:
	Terminal(); ///<�������������� ��������� ������ �� ���������
	~Terminal(); ///<���������� ���������� ������
	/*!
	���������� ������
	\param storage ��������� �� ������� ��������� �����
	\return id ������������ ������
	*/
	int AddStorage(Storage* storage);
	/*!
	�������� ������
	\param id id ���������� ������
	\return true, ���� ����� ������� �����
	*/
	bool RemoveStorage(int id);
	/*!
	��������� ��������� �� ����� �� ��� id
	\param id id ������
	\return ��������� �� �����
	*/
	Storage* GetStorage(int id) const;
	//bool SetStorageSize(int id, Point size);

	/*!
	�������� ������ ���������� � � ��������� � �����
	\param os ������ �� �����
	\param term ������ �� ��������
	\return ������ �� �������� �����
	*/
	friend std::ostream& operator << (std::ostream& os, const Terminal& term);

private:
	std::vector<Storage*> storages; ///<������ �������
};