#include "Box.h"

Box::Box(Point size, float m, const std::string& firm, int price)
	: id(0)
{
	this->size = size;
	this->m = m;
	this->firm = firm;
	this->price = 100;
}

Point Box::GetRotatedWHD() const
{
	Point rotatedsz = size;
	if (rotation.x)
	{
		Point temprotated = rotatedsz;
		rotatedsz.y = temprotated.z;
		rotatedsz.z = temprotated.y;
	}
	if (rotation.y)
	{
		Point temprotated = rotatedsz;
		rotatedsz.x = temprotated.z;
		rotatedsz.z = temprotated.x;
	}
	if (rotation.z)
	{
		Point temprotated = rotatedsz;
		rotatedsz.x = temprotated.y;
		rotatedsz.y = temprotated.x;
	}
	return rotatedsz; 
}

int Box::GetID() const
{
	return id;
}

bool Box::CheckT(int t) const
{
	return true;
}

bool Box::CheckUpperMass(float m) const
{
	return true;
}

void Box::GetAABB(Point& a, Point& b) const
{
	Point rotatedwdh = GetRotatedWHD();
	
	a = pos;
	b = pos;

	a.x -= int((rotatedwdh.x - 1) / 2);
	b.x += int((rotatedwdh.x + 2) / 2);

	a.y -= int((rotatedwdh.y - 1) / 2);
	b.y += int((rotatedwdh.y + 2) / 2);

	a.z -= int((rotatedwdh.z - 1) / 2);
	b.z += int((rotatedwdh.z + 2) / 2);
}

float Box::GetDensity() const
{
	int volume = size.x * size.y * size.z;
	return m / volume;
}

Box* Box::Clone() const
{
	return new Box(*this);
}

void Box::SetRotation(Point angle)
{
	rotation = angle;
}

void Box::SetPosition(Point p)
{
	pos = p; 
}

void Box::SetID(int ID)
{
	id = ID;
}

int Box::Type() const
{
	return 0;
}

Point Box::GetPosition() const
{
	return pos;
}

Point Box::GetRotation() const
{
	return rotation;
}

std::ostream& Box::Print(std::ostream& os) const
{
	os << "Box:" << std::endl;
	os << "id" << " " << "Position" << " " << "Size" << " " << "Rotation" << " " << "Mass" << " " << "Firm" << " " << "Price" << " " << "MaxT" << " " << "MaxUM" << std::endl;
	os << id << " " << pos << " " << size << " " << rotation << " " << m << " " << firm << " " << price << " ";
	this->PrintAdd(os);
	os << std::endl;
	return os;
}

std::ostream &Box::PrintAdd(std::ostream &os) const
{
	os << "No " << "No ";
	return  os;
}