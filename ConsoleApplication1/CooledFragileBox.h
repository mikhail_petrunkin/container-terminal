#pragma once

#include "FragileBox.h"
#include "CooledBox.h"

/**����������� �����, ����������� ������� ����������� ���������*/
class CooledFragileBox : public CooledBox, public FragileBox
{
public:
	CooledFragileBox(Point size, float m, const std::string& firm, int price, float maxUpperMass, int t);
	Box* Clone() const;
	int Type() const;
	std::ostream& PrintAdd(std::ostream& os) const;
};