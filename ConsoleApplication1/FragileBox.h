#pragma once

#include "Box.h"

/**����������� �����, ����������� ������� ���������*/
class FragileBox : virtual public Box
{
public:
	FragileBox(Point size, float m, const std::string& firm, int price, float maxUpperMass);
	Box* Clone() const;
	bool CheckUpperMass(float m) const override;
	void SetRotation(Point angle) override;
	int Type() const;
	std::ostream& PrintAdd(std::ostream& os) const;
protected:
	float maxUpperMass; ///>����������� ���������� �����
};

