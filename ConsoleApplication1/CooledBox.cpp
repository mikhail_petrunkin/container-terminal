#include "CooledBox.h"

CooledBox::CooledBox(Point size, float m, const std::string& firm, int price, int t)
	: Box(size, m,firm,price)
{
	temperature = t;
}

Box* CooledBox::Clone() const
{
	return new CooledBox(*this);
}

bool CooledBox::CheckT(int t) const
{
	if (t <= temperature)
		return true;
	else
		return false;
}

int CooledBox::Type() const
{
	return 1;
}

std::ostream& CooledBox::PrintAdd(std::ostream& os) const
{
os<<temperature<<" " << "No";;
	return os;
}

