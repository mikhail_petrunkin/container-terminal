#include "..\ConsoleApplication1\Terminal.h"
#include "..\ConsoleApplication1\Box.h"
#include "..\ConsoleApplication1\CooledFragileBox.h"
using namespace std;

template <class MyType>
int getNum(MyType &a)
{
	std::cin >> a;
	if (!std::cin.good())
		return -1;
}

int ContBufer(int i, Box*& box)
{
	if (i == 0)
		box = 0;
	if (i == 1)
	{
		int type;
		Point p; float m;
		std::cout << "������� ������: ";
		std::cin >> p;
		std::cout << "������� �����: ";
		std::cin >> m;
		
		std::cout << "�������� ��� ����������: (0 - �������, 1 - �����������, 2 - �������, 3 - ����������� �������)";

		if (getNum(type) < 0)
		{
			std::cout << "������ �����";
			return 0;
		}
		if (type == 0)
		{
			box = new Box(p, m, "Box", 100);
		}
		if (type == 1)
		{
			int t;
			std::cout << "������� ����������� ���������� �����������: ";
			std::cin >> t;
			box = new CooledBox(p, m, "CBox", 100, t);
		}
		if (type == 2)
		{
			float maxUpperMas;
			std::cout << "������� ����������� ���������� �����: ";
			std::cin >> maxUpperMas;
			box = new FragileBox(p, m, "FBox", 100, maxUpperMas);
		}
		if (type == 3)
		{
			int t;
			std::cout << "������� ����������� ���������� �����������: ";
			std::cin >> t;
			float maxUpperMas;
			std::cout << "������� ����������� ���������� �����: ";
			std::cin >> maxUpperMas;
			box = new CooledFragileBox(p, m, "CFBox", 100, maxUpperMas, t);
		}
		box->Print(std::cout);
	}
}

int main()
{
	setlocale(LC_ALL, "rus");
	Terminal term;
	Box* box=0;
	Storage * storage = 0;
	int storage_id = 0;
	const char *msgs[] = {
		"1. �������� �����",
		"2. ����� ���������� � ���������",
		"3. �������/�������� ���������",
		"4. ������� ����� �� id",
		"5. ��������� ��������� �� �����",
		"6. ����� ���������� � ������",
		"7. �������� ������ ������",
		"8. ������� �����",
		"9. ���������� ������������ ���������� �����������",
		"10. ������� ���������",
		"11. ��������� ���������",
		"12. ����������� ���������",
		"0. �����",
	}; 
	const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
	const std::string empty= "������ � �������� id �� ���������� ��� ����� �� ������";
	int rc;
	int c = 1;
	do
	{
		for (int i = 0; i < NMsgs; ++i)
			puts(msgs[i]);
		std::cout << "�������� ��������" << std::endl;
		if (getNum(rc) < 0)
		{
			std::cout << "������ �����";
			return 0;
		}
		switch (rc)
		{
		case 1:
		{
			Point p;
			std::cout << "������� ������: ";
			std::cin >> p;
			int t;
			std::cout << "������� �����������: ";
			std::cin >> t;
			Storage* storage = new Storage(p, t);
			term.AddStorage(storage);
			break;
		}
		case 2:
			std::cout << term;
			break;
		case 3:
		{
			std::cout << "������� - 0, �������� - 1 ";
			int type;
			std::cin >> type;
			if (type == 0)
				ContBufer(0, box);
			if (type == 1)
				ContBufer(1, box);
			break;
		}
		case 4:
		{
			std::cout << "������� id ������: ";
			int id;
			std::cin >> id;
			Storage* st = term.GetStorage(id);
			storage = st;
			if (st)
				storage_id = id;
			break;
		}
		case 5:
		{
			if (!box)
			{
				std::cout << "��������� �� ������" << std::endl;;
				break;
			}
			if (!storage)
				std::cout << empty << std::endl;
			else
			{
			std::cout << ("0-������ �������� �����, 1-�������������� ");
			int type;
			std::cin >> type;
			int boxid;
				if (type == 0)
				{
					Point p;
					std::cout << "������� ���������� ������: ";
					std::cin >> p;
					boxid = storage->AddBox(box->Clone(), p, Point(0, 0, 0));
				}
				if (type == 1)
					boxid = storage->AddBox(box->Clone());

				if (boxid >= 0)
				{
					std::cout << "���������� �������� id " << boxid << std::endl;;

				}
				else std::cout << "�� ������� ��������� ��������� �� �����" << std::endl;;
			}
			break;
		}
		case 6:
		{
			if (storage)
			std::cout << *storage;
			break;
		}
		case 7:
		{
			if (!storage)
				std::cout << empty << std::endl;
			if (storage)
			{
				Point p;
				std::cout << "������� ����� ������: ";
				std::cin >> p;
				int res =storage->Resize(p);
				if (!res) std::cout << "�������� ������ ������ �� �������";
				else std::cout << "������ �������";
			}
				break;
		}
		case 8:
		{
			if (!storage)
				std::cout << empty<< std::endl;
			if (storage)
			{
				int res = term.RemoveStorage(storage_id);
				if (res)
					std::cout << "����� �����" << std::endl;
				else
					std::cout << "FALSE";
				storage = 0;
				storage_id = 0;
			}
			break;
		}
		case 9:
		{
			if (!storage)
				std::cout << empty << std::endl;
			if (storage)
			{
				std::cout << "1 - � ���������, 0 - ��� �������� ";
				int allow;
				int res=987;
				if (getNum(allow) < 0)
				{
					std::cout << "������ �����";
					return 0;
				}
				std::cout << allow;
				std::cout << "������� ������ ����������: " << std::endl;
				Point size;
				std::cin >> size;
				if (allow == 0)
					res = storage->GetMaxPossibleCountOf(size, false);
				if (allow == 1)
					res = storage->GetMaxPossibleCountOf(size, true);
				std::cout << "max=" << res << endl;;
			}
			break;
		}
		case 10:
		{
			int id;
			std::cout << "������� id ����������: ";
			if (getNum(id) < 0)
			{
				std::cout << "������ �����";
				return 0;
			}
			storage->RemoveBox(id);
			std::cout << "��������� �����"<<std::endl;
		}
		break;
		case 11:
		{
			int id;
			std::cout << "������� id ����������: ";
			if (getNum(id) < 0)
			{
				std::cout << "������ �����";
				return 0;
			}
			std::cout << "��������� ��������: ";
			Point angle;
			std::cin >> angle;
			if (!storage->RotateBox(id, angle))
				std::cout << "��������� �� ���������� ��� �� ����� ���� �������";
			else
				std::cout << "��������� � " << id << "�������";
		}
		break;
		case 12:
		{
			int id;
			std::cout << "������� id ����������: ";
			if (getNum(id) < 0)
			{
				std::cout << "������ �����";
				return 0;
			}
			std::cout << "����� ����������: ";
			Point pos;
			std::cin >> pos;
			if (!storage->MoveBox(id, pos))
				std::cout << "��������� �� ���������� ��� �� ����� ���� ���������" << std::endl;
			else
				std::cout << "��������� � " << id << "���������";
		}
		break;
		case 0:
		{
			c = 0;
			break;
		}
		default:
			std::cout << "�������� � ����� ������� �� �������������" << std::endl;
			break;
		}
	} while (c == 1);

//Terminal terminal;
//int myStorage = terminal.AddStorage(new Storage(Point( 1, 2, 3 ), 0));
//terminal.GetStorage(myStorage)->AddBox(new Box({ 3, 1, 1 }, 1, "", 0));
//int myBox = terminal.GetStorage(myStorage)->AddBox(new Box({ 1, 2, 1 }, 1, "", 0));
//Point pos = terminal.GetStorage(myStorage)->GetBox(myBox)->GetPosition();
//int myBox2 = terminal.GetStorage(myStorage)->AddBox(new Box({ 1, 1, 1 }, 1, "", 0));
//terminal.GetStorage(myStorage)->RemoveBox(myBox);
return 0;
}

